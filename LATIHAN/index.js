const fs = require("fs");
const { resolve } = require("path");
const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const pertanyaan1 = () => {
  return new Promise((resolve, reject) => {
    rl.question("masukkan username anda :", (username) => {
      resolve(username);
    });
  });
};

const pertanyaan2 = () => {
  return new Promise((resolve, reject) => {
    rl.question("masukkan email anda :", (email) => {
      resolve(email);
    });
  });
};

const pertanyaan3 = () => {
  return new Promise((resolve, reject) => {
    rl.question("masukkan password anda :", (password) => {
      resolve(password);
    });
  });
};

const main = async () => {
  const username = await pertanyaan1();
  const email = await pertanyaan2();
  const password = await pertanyaan3();

  const RegisterAkun = { username, email, password };
  const filebuffer = fs.readFileSync("./listAkun.json", "utf-8");
  const contacts = JSON.parse(filebuffer);

  contacts.push(RegisterAkun);

  fs.writeFileSync("./listAkun.json", JSON.stringify(contacts));

  console.log("terimakasih telah melakukan register");
  rl.close();
};

main();
