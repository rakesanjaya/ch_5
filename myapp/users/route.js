const express = require("express");
const userRouter = express.Router();
const UserController = require("./controller");

userRouter.get("/", UserController.getAllUsers);

userRouter.post("/register", UserController.registerUsers);

userRouter.post("/login", UserController.userLogin);

userRouter.post("/registerbio", UserController.registerBio);

userRouter.get("/finduserbio/:idUser", UserController.getsingleuserbio);

userRouter.get("/detail/:idUser", UserController.getSingleUser);

userRouter.put("/detail/:idUser", UserController.updateUserBio);

userRouter.put("/gamehistory", UserController.inputGamehistory);

userRouter.get("/gamehistory/:idUser", UserController.getsingleuserhistory);

module.exports = userRouter;
