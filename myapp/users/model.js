const md5 = require("md5");
const db = require("../db/models");
const { Op } = require("sequelize");

const daftarUser = [];

class UserModel {
  // dapatkan semua data
  getAllUser = async () => {
    const daftarUser = await db.User.findAll({
      include: [db.UserBio, db.GameHistory],
    });
    // SELECT * FROM users
    return daftarUser;
  };
  // mencari 1 data(single user)
  getSingleUser = async (idUser) => {
    return await db.User.findOne({ where: { id: idUser } });
  };

  getsingleuserbio = async (idUser) => {
    return await db.User.findOne({
      include: [db.UserBio],
      where: { id: idUser },
    });
  };
  updateUserBio = async (idUser, fullname, PhoneNumber, Address) => {
    return await db.UserBio.update(
      { fullname: fullname, PhoneNumber: PhoneNumber, Address: Address },
      { where: { User_id: idUser } }
    );
  };

  // updateGameHistory = async (idUser, status) => {
  //   return await db.GameHistory.update(
  //     { status: status },
  //     { where: { User_id: idUser } }
  //   );
  // };

  //   method check username terregister atau tidak
  isUserRegistered = async (dataRequest) => {
    const existData = await db.User.findOne({
      where: {
        [Op.or]: [
          { username: dataRequest.username },
          { email: dataRequest.email },
        ],
      },
    });
    if (existData) {
      return true;
    } else {
      return false;
    }
  };

  // method check data bio
  isUserBioRegistered = async (dataBio) => {
    const existBio = await db.UserBio.findOne({
      where: {
        [Op.or]: [
          { fullname: dataBio.fullname },
          { PhoneNumber: dataBio.PhoneNumber },
          { Address: dataBio.Address },
          { User_id: dataBio.User_id },
        ],
      },
    });
    if (existBio) {
      return true;
    } else {
      return false;
    }
  };

  // userSudahpernahmain = async (dataHistory) => {
  //   const sudahpernahmain = await db.GameHistory.findOne({
  //     where: {
  //       [Op.or]: [{ status: dataHistory.status }],
  //     },
  //   });
  //   if (sudahpernahmain) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // };

  // method record data
  recordNewdata = (dataRequest) => {
    // INSERT INTO table () values()
    db.User.create({
      username: dataRequest.username,
      email: dataRequest.email,
      password: md5(dataRequest.password),
      fullname: dataRequest.fullname,
    });
  };
  recordNewdatabio = (dataBio) => {
    db.UserBio.create({
      fullname: dataBio.fullname,
      PhoneNumber: dataBio.PhoneNumber,
      Address: dataBio.Address,
      User_id: dataBio.User_id,
    });
  };
  recordNewdatahistory = (dataHistory) => {
    db.GameHistory.create({
      User_id: dataHistory.User_id,
      status: dataHistory.status,
    });
  };

  getsingleuserhistory = async (idUser) => {
    return await db.User.findOne({
      include: [db.GameHistory],
      where: { id: idUser },
    });
  };

  // login
  verifyLogin = async (username, password) => {
    const dataUser = await db.User.findOne({
      where: { username: username, password: md5(password) },
    });

    return dataUser;
  };
}
module.exports = new UserModel();
